What can be found in this project:

1. `acidabse.sql` file which holds the SQL statements to create the database's
structure.
2. `Acidbase.dbm` file which is a model file for the magnificent [pgModeler] [1]
software. The scripts within `acidabse.sql` file are exported from this one.

  [1]: https://github.com/pgmodeler/pgmodeler