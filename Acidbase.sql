-- Prepended SQL commands --
CREATE EXTENSION dblink;
---

-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.8.2
-- PostgreSQL version: 9.5
-- Project Site: pgmodeler.com.br
-- Model Author: Mehran Ziadloo

SET check_function_bodies = false;
-- ddl-end --


-- Database creation must be done outside an multicommand file.
-- These commands were put in this file only for convenience.
-- -- object: "Acidbase" | type: DATABASE --
-- -- DROP DATABASE IF EXISTS "Acidbase";
-- CREATE DATABASE "Acidbase"
-- 	TEMPLATE = template0
-- 	ENCODING = 'UTF8'
-- 	LC_COLLATE = 'en_US.UTF8'
-- 	LC_CTYPE = 'en_US.UTF8'
-- 	TABLESPACE = pg_default
-- 	OWNER = postgres
-- ;
-- -- ddl-end --
-- 

-- object: "Acidbase" | type: SCHEMA --
-- DROP SCHEMA IF EXISTS "Acidbase" CASCADE;
CREATE SCHEMA "Acidbase";
-- ddl-end --
ALTER SCHEMA "Acidbase" OWNER TO postgres;
-- ddl-end --

SET search_path TO pg_catalog,public,"Acidbase";
-- ddl-end --

-- object: "Acidbase"."REVISION_STATE" | type: TYPE --
-- DROP TYPE IF EXISTS "Acidbase"."REVISION_STATE" CASCADE;
CREATE TYPE "Acidbase"."REVISION_STATE" AS
 ENUM ('Active','Obsolete','Deleted','Revived');
-- ddl-end --
ALTER TYPE "Acidbase"."REVISION_STATE" OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".object | type: TABLE --
-- DROP TABLE IF EXISTS "Acidbase".object CASCADE;
CREATE TABLE "Acidbase".object(
	object_id bigserial NOT NULL,
	fields jsonb NOT NULL,
	CONSTRAINT pk_3 PRIMARY KEY (object_id)

);
-- ddl-end --
ALTER TABLE "Acidbase".object OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".revision | type: TABLE --
-- DROP TABLE IF EXISTS "Acidbase".revision CASCADE;
CREATE TABLE "Acidbase".revision(
	revise_date timestamp NOT NULL DEFAULT current_timestamp,
	entity_id bigint NOT NULL,
	revision_state "Acidbase"."REVISION_STATE" NOT NULL DEFAULT 'Active',
	reviser bigint NOT NULL,
-- 	object_id bigint NOT NULL,
-- 	fields jsonb NOT NULL,
	CONSTRAINT revision_pk PRIMARY KEY (object_id)

) INHERITS("Acidbase".object)
;
-- ddl-end --
ALTER TABLE "Acidbase".revision OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".entity | type: TABLE --
-- DROP TABLE IF EXISTS "Acidbase".entity CASCADE;
CREATE TABLE "Acidbase".entity(
	entity_id bigserial NOT NULL,
	entity_type varchar(32) NOT NULL,
	create_date timestamp NOT NULL DEFAULT current_timestamp,
	creator bigint NOT NULL,
	CONSTRAINT pk PRIMARY KEY (entity_id)

);
-- ddl-end --
ALTER TABLE "Acidbase".entity OWNER TO postgres;
-- ddl-end --

-- object: entity_fk | type: CONSTRAINT --
-- ALTER TABLE "Acidbase".revision DROP CONSTRAINT IF EXISTS entity_fk CASCADE;
ALTER TABLE "Acidbase".revision ADD CONSTRAINT entity_fk FOREIGN KEY (entity_id)
REFERENCES "Acidbase".entity (entity_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: "Acidbase".collection | type: TABLE --
-- DROP TABLE IF EXISTS "Acidbase".collection CASCADE;
CREATE TABLE "Acidbase".collection(
	collection_id bigserial NOT NULL,
	name varchar(32) NOT NULL,
	object_id bigint NOT NULL,
	CONSTRAINT pk_1 PRIMARY KEY (collection_id)

);
-- ddl-end --
ALTER TABLE "Acidbase".collection OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase"."user" | type: TABLE --
-- DROP TABLE IF EXISTS "Acidbase"."user" CASCADE;
CREATE TABLE "Acidbase"."user"(
	user_id bigserial NOT NULL,
	username varchar(100) NOT NULL,
	password varchar(100) NOT NULL,
	CONSTRAINT pk_5 PRIMARY KEY (user_id)

);
-- ddl-end --
ALTER TABLE "Acidbase"."user" OWNER TO postgres;
-- ddl-end --

-- Appended SQL commands --
INSERT INTO "Acidbase"."user" (user_id,username,password) VALUES (DEFAULT, 'root', '123');
-- ddl-end --

-- object: "Acidbase".item | type: TABLE --
-- DROP TABLE IF EXISTS "Acidbase".item CASCADE;
CREATE TABLE "Acidbase".item(
	collection_id bigint NOT NULL,
	revision_id bigint,
-- 	object_id bigint NOT NULL,
-- 	fields jsonb NOT NULL,
	CONSTRAINT item_pk PRIMARY KEY (object_id)

) INHERITS("Acidbase".object)
;
-- ddl-end --
ALTER TABLE "Acidbase".item OWNER TO postgres;
-- ddl-end --

-- object: collection_fk | type: CONSTRAINT --
-- ALTER TABLE "Acidbase".item DROP CONSTRAINT IF EXISTS collection_fk CASCADE;
ALTER TABLE "Acidbase".item ADD CONSTRAINT collection_fk FOREIGN KEY (collection_id)
REFERENCES "Acidbase".collection (collection_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: revision_fk | type: CONSTRAINT --
-- ALTER TABLE "Acidbase".item DROP CONSTRAINT IF EXISTS revision_fk CASCADE;
ALTER TABLE "Acidbase".item ADD CONSTRAINT revision_fk FOREIGN KEY (revision_id)
REFERENCES "Acidbase".revision (object_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: public.json_merge | type: FUNCTION --
-- DROP FUNCTION IF EXISTS public.json_merge(json,json) CASCADE;
CREATE FUNCTION public.json_merge ( data json,  merge_data json)
	RETURNS json
	LANGUAGE sql
	IMMUTABLE 
	RETURNS NULL ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
SELECT ('{'||string_agg(to_json(key)||':'||value, ',')||'}')::json
FROM (
    WITH to_merge AS (
        SELECT * FROM json_each(merge_data)
    )
    SELECT *
    FROM json_each(data)
    WHERE key NOT IN (SELECT key FROM to_merge)
    UNION ALL
    SELECT * FROM to_merge
) t;
$$;
-- ddl-end --
ALTER FUNCTION public.json_merge(json,json) OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".extract_revision | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".extract_revision(IN bigint,IN jsonb) CASCADE;
CREATE FUNCTION "Acidbase".extract_revision (IN p_revision_id bigint, IN structure jsonb)
	RETURNS jsonb
	LANGUAGE plpgsql
	VOLATILE 
	RETURNS NULL ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
    rev jsonb;
BEGIN
    SELECT
        jsonb_build_object(
            'entity_id', e.entity_id,
            'revision_id', r.object_id,
            'entity_type', e.entity_type,
            'create_date', e.create_date,
            'revise_date', r.revise_date,
            'creator', e.creator,
            'reviser', r.reviser,
            'revision_state', r.revision_state
        )
    INTO rev
    FROM "Acidbase".entity e
    INNER JOIN "Acidbase".revision r ON (e.entity_id = r.entity_id)
    WHERE
        r.object_id = p_revision_id;

    RETURN "Acidbase".extract_object(p_revision_id, structure, rev);
END;

$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".extract_revision(IN bigint,IN jsonb) OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase"."EXTRACTION_STATE" | type: TYPE --
-- DROP TYPE IF EXISTS "Acidbase"."EXTRACTION_STATE" CASCADE;
CREATE TYPE "Acidbase"."EXTRACTION_STATE" AS
 ENUM ('Latest','LatestActive');
-- ddl-end --
ALTER TYPE "Acidbase"."EXTRACTION_STATE" OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".extract_collection | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".extract_collection(IN bigint,IN varchar(32),IN jsonb,IN "Acidbase"."EXTRACTION_STATE") CASCADE;
CREATE FUNCTION "Acidbase".extract_collection (IN p_object_id bigint, IN p_name varchar(32), IN structure jsonb, IN extract_state "Acidbase"."EXTRACTION_STATE" DEFAULT 'LatestActive')
	RETURNS jsonb
	LANGUAGE plpgsql
	VOLATILE 
	RETURNS NULL ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
    obj jsonb;
    col jsonb[];
    rec record;
BEGIN
    col := ARRAY[]::jsonb[];

    -- Finding a forward relation
    FOR rec IN
        SELECT
            c.collection_id, i.object_id, i.revision_id
        FROM "Acidbase".collection c
        INNER JOIN "Acidbase".item i ON (c.collection_id = i.collection_id)
        WHERE
            c.object_id = p_object_id
        AND
            c.name = p_name
    LOOP
        SELECT *
        INTO obj
        FROM "Acidbase".extract_collection_1(rec.object_id, rec.revision_id, structure);

        IF obj IS NOT NULL THEN
            SELECT *
            INTO col
            FROM array_append(col, obj);
        END IF;
    END LOOP;
    
    -- In case there's been no forward relation, look for a backward
    IF col = '{}' THEN
        FOR rec IN
            SELECT
                c.collection_id, r.object_id, r.object_id AS revision_id
            FROM "Acidbase".collection c
            INNER JOIN "Acidbase".item i ON (c.collection_id = i.collection_id)
            INNER JOIN "Acidbase".revision r ON (c.object_id = r.object_id)
            WHERE
                i.revision_id = p_object_id
            AND
                c.name = p_name
        LOOP
            SELECT *
            INTO obj
            FROM "Acidbase".extract_collection_1(rec.object_id, rec.revision_id, structure);

            IF obj IS NOT NULL THEN
                SELECT *
                INTO col
                FROM array_append(col, obj);
            END IF;
        END LOOP;
    END IF;

    RETURN to_jsonb(col);
END;

$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".extract_collection(IN bigint,IN varchar(32),IN jsonb,IN "Acidbase"."EXTRACTION_STATE") OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase"."ENTITY_REVISION_INFO" | type: TYPE --
-- DROP TYPE IF EXISTS "Acidbase"."ENTITY_REVISION_INFO" CASCADE;
CREATE TYPE "Acidbase"."ENTITY_REVISION_INFO" AS
(
  entity_id bigint,
  revision_id bigint,
  entity_type varchar(32),
  create_date timestamp,
  revise_date timestamp,
  creator bigint,
  reviser bigint,
  revision_state "Acidbase"."REVISION_STATE"
);
-- ddl-end --
ALTER TYPE "Acidbase"."ENTITY_REVISION_INFO" OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".store_entity | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".store_entity(bigint,varchar(32),bigint,jsonb) CASCADE;
CREATE FUNCTION "Acidbase".store_entity ( p_entity_id bigint,  p_entity_type varchar(32),  p_creator bigint,  data jsonb)
	RETURNS "Acidbase"."ENTITY_REVISION_INFO"
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
    rec record;
    eri "Acidbase"."ENTITY_REVISION_INFO";
BEGIN
    IF p_entity_id IS NULL THEN
        INSERT INTO "Acidbase".entity
            (entity_id, entity_type, create_date, creator)
        VALUES
            (DEFAULT, p_entity_type, DEFAULT, p_creator);

        p_entity_id := LASTVAL();
    ELSE
        UPDATE "Acidbase".revision
        SET
            revision_state = 'Obsolete'
        WHERE
            entity_id = p_entity_id
        AND
            revision_state = 'Active';

        UPDATE "Acidbase".revision
        SET
            revision_state = 'Revived'
        WHERE
            entity_id = p_entity_id
        AND
            revision_state = 'Deleted';
    END IF;
    
    SELECT
        entity_id,
        creator,
        create_date,
        entity_type
    INTO
        eri.entity_id,
        eri.creator,
        eri.create_date,
        eri.entity_type
    FROM "Acidbase".entity
    WHERE
        entity_id = p_entity_id;
    
    IF (data->'fields') IS NULL OR jsonb_typeof(data->'fields') <> 'object' THEN
        data := data || '{"fields": {}}'::jsonb;
    END IF;

    IF (data->'collections') IS NULL OR jsonb_typeof(data->'collecitons') <> 'array' THEN
        data := data || '{"collections": {}}'::jsonb;
    END IF;

    INSERT INTO "Acidbase".revision
        (object_id, entity_id, reviser, revise_date, revision_state, fields)
    VALUES
        (DEFAULT, p_entity_id, p_creator, DEFAULT, 'Active', data->'fields');

    eri.revision_id := LASTVAL();

    SELECT
        reviser,
        revise_date,
        revision_state
    INTO
        eri.reviser,
        eri.revise_date,
        eri.revision_state
    FROM "Acidbase".revision
    WHERE
        object_id = eri.revision_id;

    FOR rec IN
    SELECT key, value
    FROM jsonb_each(data->'collections') AS t
    LOOP
        IF jsonb_typeof(rec.value) = 'array' THEN
            PERFORM "Acidbase".store_collection(eri.revision_id, rec.key, rec.value);
        END IF;
    END LOOP;
    
    RETURN eri;
END;
$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".store_entity(bigint,varchar(32),bigint,jsonb) OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".store_collection | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".store_collection(bigint,varchar(32),jsonb) CASCADE;
CREATE FUNCTION "Acidbase".store_collection ( p_object_id bigint,  p_name varchar(32),  items jsonb)
	RETURNS bigint
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
    p_collection_id bigint;
    rec record;
    sub_rec record;
    sub_object_id bigint;
BEGIN
    IF jsonb_typeof(items) <> 'array' THEN
        RAISE EXCEPTION 'The collection items named `%` need to be an array but % is given', p_name, jsonb_typeof(items);
    END IF;

    DELETE FROM "Acidbase".collection WHERE name = p_name AND object_id = p_object_id;

    INSERT INTO "Acidbase".collection
        (collection_id, name, object_id)
    VALUES
        (DEFAULT, p_name, p_object_id);

    p_collection_id := LASTVAL();

    FOR rec IN
    SELECT value
    FROM jsonb_array_elements(items) AS t
    LOOP
        IF jsonb_typeof(rec.value) <> 'object'
        OR (
            (
                (rec.value->'fields') IS NULL
            OR
                jsonb_typeof(rec.value->'fields') <> 'object'
            OR
                rec.value->'fields'::text = '{}'
            )
            AND
                (rec.value->'revision_id') IS NULL
            AND
                (rec.value->'entity_id') IS NULL
        ) THEN
            RAISE EXCEPTION 'Each item within `%` collection needs to be an object with either `fields`, `revision_id` or `entity_id` property', p_name;
        END IF;
        
        IF rec.value->'fields' IS NULL OR jsonb_typeof(rec.value->'fields') <> 'object' THEN
            rec.value := rec.value || '{"fields": {}}'::jsonb;
        END IF;
        
        IF rec.value->'collections' IS NULL OR jsonb_typeof(rec.value->'collecitons') <> 'array' THEN
            rec.value := rec.value || '{"collecitons": {}}'::jsonb;
        END IF;

        IF (rec.value->'revision_id') IS NOT NULL THEN
            INSERT INTO "Acidbase".item
                (object_id, collection_id, fields, revision_id)
            VALUES
                (DEFAULT, p_collection_id, rec.value->'fields', (rec.value->>'revision_id')::bigint);
        ELSEIF (rec.value->'entity_id') IS NOT NULL THEN
            INSERT INTO "Acidbase".item
                (collection_id, fields, revision_id)
            SELECT
                p_collection_id, rec.value->'fields', object_id
            FROM "Acidbase".revision
            WHERE
                entity_id = (rec.value->>'entity_id')::bigint
            AND
                revision_state IN ('Active', 'Deleted');
        ELSE
            INSERT INTO "Acidbase".item
                (object_id, collection_id, fields, revision_id)
            VALUES
                (DEFAULT, p_collection_id, rec.value->'fields', NULL);
        END IF;

        sub_object_id := LASTVAL();

        FOR sub_rec IN
        SELECT key, value
        FROM jsonb_each(rec.value->'collecitons') AS t
        LOOP
            IF jsonb_typeof(sub_rec.value) = 'array' THEN
                PERFORM "Acidbase".store_collection(sub_object_id, sub_rec.key, sub_rec.value);
            END IF;
        END LOOP;
    END LOOP;

    RETURN p_collection_id;
END;
$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".store_collection(bigint,varchar(32),jsonb) OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".extract_entity_by_revision | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".extract_entity_by_revision(IN bigint,IN jsonb,IN "Acidbase"."EXTRACTION_STATE") CASCADE;
CREATE FUNCTION "Acidbase".extract_entity_by_revision (IN p_revision_id bigint, IN structure jsonb, IN state "Acidbase"."EXTRACTION_STATE" DEFAULT 'Latest')
	RETURNS jsonb
	LANGUAGE plpgsql
	VOLATILE 
	RETURNS NULL ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
    revision_id bigint;
    states "Acidbase"."REVISION_STATE"[];
BEGIN
    states[1] := 'Active';
    IF "state" = 'Latest' THEN
        states[2] := 'Deleted';
    ELSE
    END IF;

    SELECT r2.object_id
    INTO revision_id
    FROM "Acidbase".revision r1
    INNER JOIN "Acidbase".revision r2 ON (r1.entity_id = r2.entity_id)
    WHERE
        r1.object_id = p_revision_id
    AND
        r2.revision_state = ANY(states);

    RETURN "Acidbase".extract_revision(revision_id, "structure");
END;

$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".extract_entity_by_revision(IN bigint,IN jsonb,IN "Acidbase"."EXTRACTION_STATE") OWNER TO postgres;
-- ddl-end --

-- object: "inxObjectId" | type: INDEX --
-- DROP INDEX IF EXISTS "Acidbase"."inxObjectId" CASCADE;
CREATE INDEX "inxObjectId" ON "Acidbase".collection
	USING btree
	(
	  object_id ASC NULLS LAST
	);
-- ddl-end --

-- object: "Acidbase".update_object | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".update_object(bigint,jsonb) CASCADE;
CREATE FUNCTION "Acidbase".update_object ( p_object_id bigint,  p_fields jsonb)
	RETURNS void
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN
    SELECT *
    INTO p_fields
    FROM "public".jsonb_object_delete_keys(p_fields, 'entity_id', 'revision_id', 'entity_type', 'create_date', 'revise_date', 'creator', 'reviser', 'revision_state');

    UPDATE "Acidbase".object
    SET
        fields = p_fields
    WHERE
        object_id = p_object_id;
END;
$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".update_object(bigint,jsonb) OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".queue_revision | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".queue_revision() CASCADE;
CREATE FUNCTION "Acidbase".queue_revision ()
	RETURNS trigger
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
    _action "Acidbase"."ACTION_TYPE";
    latest_states "Acidbase"."REVISION_STATE"[];
BEGIN
    IF "public".get_config('acidbase.external_indexing', 'false') = 'true' AND (TG_OP = 'INSERT' OR NEW.fields != OLD.fields) THEN
        latest_states := array(
            SELECT r.revision_state
            FROM "Acidbase".revision r
            WHERE
                r.entity_id = NEW.entity_id
            ORDER BY r.revise_date DESC, r.object_id DESC
            LIMIT 2
        );

        IF TG_OP = 'INSERT' AND array_length(latest_states, 1) = 1 THEN
            _action = 'Add';
        ELSEIF TG_OP = 'INSERT' AND array_length(latest_states, 1) = 2 THEN
            IF latest_states[1] = 'Active' AND latest_states[2] = 'Obsolete' THEN
                _action = 'Revise';
            ELSEIF latest_states[1] = 'Active' AND latest_states[2] = 'Revived' THEN
                _action = 'Revive';
            ELSE
                _action = 'Delete';
            END IF;
        ELSEIF TG_OP = 'UPDATE' THEN
            _action = 'Edit';
        END IF;

        CREATE TEMPORARY TABLE IF NOT EXISTS change_queue_tmp(
            entity_id bigint NOT NULL,
            revision_id bigint NOT NULL,
            action varchar(32) NOT NULL
        );
        
        INSERT INTO change_queue_tmp
            (entity_id, revision_id, "action")
        VALUES
            (NEW.entity_id, NEW.object_id, _action);
    END IF;

    RETURN NEW;
END;

$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".queue_revision() OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".extract_object | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".extract_object(IN bigint,IN jsonb,IN jsonb) CASCADE;
CREATE FUNCTION "Acidbase".extract_object (IN p_object_id bigint, IN structure jsonb, IN initial_fields jsonb DEFAULT NULL)
	RETURNS jsonb
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
    object_fields jsonb;
    filtered_fields jsonb;
    collection jsonb;
    rec record;
    collections jsonb;
BEGIN
    SELECT
        "Acidbase".object.fields
    INTO
        object_fields
    FROM "Acidbase".object
    WHERE
        object_id = p_object_id;

    IF object_fields IS NULL THEN
        RETURN NULL;
    ELSE
        filtered_fields := '{}'::jsonb;

        FOR rec IN
        SELECT key, value
        FROM jsonb_each(object_fields) AS t
        WHERE lower(t.key) = ANY (SELECT lower(value) FROM jsonb_array_elements_text("structure"->'fields'))
        LOOP
            filtered_fields := filtered_fields || jsonb_build_object(rec.key, rec.value);
        END LOOP;
    END IF;

    IF initial_fields IS NULL OR jsonb_typeof(initial_fields) <> 'object' THEN
        initial_fields := '{}'::jsonb;
    END IF;

    initial_fields := initial_fields || jsonb_build_object('fields', filtered_fields);
    
    IF NOT(initial_fields ? 'revision_id') THEN
        initial_fields := initial_fields || jsonb_build_object('object_id', p_object_id);
    END IF;

    collections := '{}'::jsonb;

    IF structure ? 'collections' AND jsonb_typeof(structure->'collections') = 'object' THEN
        FOR rec IN
        SELECT key, value
        FROM jsonb_each(structure->'collections') AS t
        LOOP
            IF jsonb_typeof(rec.value) = 'object' THEN
                SELECT *
                INTO collection
                FROM "Acidbase".extract_collection(p_object_id, rec.key, rec.value);

                collections := collections || jsonb_build_object(rec.key, collection);
            END IF;
        END LOOP;
    END IF;
    
    initial_fields := initial_fields || jsonb_build_object('collections', collections);

    RETURN initial_fields;
END;

$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".extract_object(IN bigint,IN jsonb,IN jsonb) OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".cascade_object_deletion | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".cascade_object_deletion() CASCADE;
CREATE FUNCTION "Acidbase".cascade_object_deletion ()
	RETURNS trigger
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN
    DELETE
    FROM "Acidbase".collection
    WHERE
        object_id = OLD.object_id;
    RETURN OLD;
END;
$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".cascade_object_deletion() OWNER TO postgres;
-- ddl-end --

-- object: cascade_object_deletion | type: TRIGGER --
-- DROP TRIGGER IF EXISTS cascade_object_deletion ON "Acidbase".object CASCADE;
CREATE TRIGGER cascade_object_deletion
	AFTER DELETE 
	ON "Acidbase".object
	FOR EACH ROW
	EXECUTE PROCEDURE "Acidbase".cascade_object_deletion();
-- ddl-end --

-- object: queue_object | type: TRIGGER --
-- DROP TRIGGER IF EXISTS queue_object ON "Acidbase".revision CASCADE;
CREATE TRIGGER queue_object
	AFTER INSERT OR UPDATE
	ON "Acidbase".revision
	FOR EACH ROW
	EXECUTE PROCEDURE "Acidbase".queue_revision();
-- ddl-end --

-- object: "Acidbase".queue_item | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".queue_item() CASCADE;
CREATE FUNCTION "Acidbase".queue_item ()
	RETURNS trigger
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
    rec record;
    entity_id_ bigint;
    revision_id_ bigint;
    count integer;
BEGIN
    IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
        rec := NEW;
    ELSE
        rec := OLD;
    END IF;

    IF "public".get_config('acidbase.external_indexing', 'false') = 'true' AND (TG_OP <> 'UPDATE' OR NEW.fields != OLD.fields) THEN
        CREATE TEMPORARY TABLE IF NOT EXISTS change_queue_tmp(
            entity_id bigint NOT NULL,
            revision_id bigint NOT NULL,
            action varchar(32) NOT NULL
        );

        SELECT
            _entity_id, _revision_id
        INTO
            entity_id_ ,revision_id_
        FROM "Acidbase".find_collection_owner(rec.collection_id);

        SELECT COUNT(*)
        INTO count
        FROM change_queue_tmp t
        WHERE
            (t.entity_id = entity_id_ AND t.action = 'Purge')
        OR
            (t.entity_id = entity_id_ AND t.revision_id = revision_id_);

        IF count = 0 THEN
            INSERT INTO change_queue_tmp
                (entity_id, revision_id, "action")
            VALUES
                (entity_id_, object_id_, 'Revise');
        END IF;
    END IF;

    RETURN rec;
END;
$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".queue_item() OWNER TO postgres;
-- ddl-end --

-- object: queue_object | type: TRIGGER --
-- DROP TRIGGER IF EXISTS queue_object ON "Acidbase".item CASCADE;
CREATE TRIGGER queue_object
	AFTER INSERT OR DELETE OR UPDATE
	ON "Acidbase".item
	FOR EACH ROW
	EXECUTE PROCEDURE "Acidbase".queue_item();
-- ddl-end --

-- object: user_fk | type: CONSTRAINT --
-- ALTER TABLE "Acidbase".entity DROP CONSTRAINT IF EXISTS user_fk CASCADE;
ALTER TABLE "Acidbase".entity ADD CONSTRAINT user_fk FOREIGN KEY (creator)
REFERENCES "Acidbase"."user" (user_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: user_fk | type: CONSTRAINT --
-- ALTER TABLE "Acidbase".revision DROP CONSTRAINT IF EXISTS user_fk CASCADE;
ALTER TABLE "Acidbase".revision ADD CONSTRAINT user_fk FOREIGN KEY (reviser)
REFERENCES "Acidbase"."user" (user_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: "Acidbase".counter | type: TABLE --
-- DROP TABLE IF EXISTS "Acidbase".counter CASCADE;
CREATE TABLE "Acidbase".counter(
	name varchar(255) NOT NULL,
	counter bigint NOT NULL DEFAULT 0,
	CONSTRAINT pk_1_1 PRIMARY KEY (name)

);
-- ddl-end --
ALTER TABLE "Acidbase".counter OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".new_entity | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".new_entity(IN varchar(32),IN bigint,IN jsonb) CASCADE;
CREATE FUNCTION "Acidbase".new_entity (IN p_entity_type varchar(32), IN p_creator bigint, IN data jsonb)
	RETURNS "Acidbase"."ENTITY_REVISION_INFO"
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
    eri "Acidbase"."ENTITY_REVISION_INFO";
BEGIN
    SELECT *
    INTO eri
    FROM "Acidbase".store_entity(NULL, "p_entity_type", "p_creator", "data");
    RETURN eri;
END;

$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".new_entity(IN varchar(32),IN bigint,IN jsonb) OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".revise_entity | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".revise_entity(IN bigint,IN bigint,IN jsonb) CASCADE;
CREATE FUNCTION "Acidbase".revise_entity (IN p_entity_id bigint, IN p_creator bigint, IN data jsonb)
	RETURNS "Acidbase"."ENTITY_REVISION_INFO"
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
    eri "Acidbase"."ENTITY_REVISION_INFO";
BEGIN
    SELECT *
    INTO eri
    FROM "Acidbase".store_entity("p_entity_id", 'unimportant', "p_creator", "data");
    RETURN eri;
END;

$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".revise_entity(IN bigint,IN bigint,IN jsonb) OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".extract_collection_1 | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".extract_collection_1(IN bigint,IN bigint,IN jsonb) CASCADE;
CREATE FUNCTION "Acidbase".extract_collection_1 (IN object_id bigint, IN revision_id bigint, IN structure jsonb)
	RETURNS jsonb
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
    obj jsonb;
    override jsonb;
    rec record;
    collections jsonb;
BEGIN
    obj := '{"fields": {}, "collections": {}}'::jsonb;

    IF revision_id IS NOT NULL THEN
        IF structure->>'revision_state' = 'Revision' THEN
            SELECT *
            INTO obj
            FROM "Acidbase".extract_revision(revision_id, structure);
        ELSEIF structure->>'revision_state' = 'Latest' THEN
            SELECT *
            INTO obj
            FROM "Acidbase".extract_entity_by_revision(revision_id, structure, 'Latest');
        ELSEIF structure->>'revision_state' = 'LatestActive' THEN
            SELECT *
            INTO obj
            FROM "Acidbase".extract_entity_by_revision(revision_id, structure, 'LatestActive');
        ELSE
            RAISE EXCEPTION 'A collection''s `revision_state` could be one of the `Revision`, `Latest` or `LatestActive`; % is given', structure->>'revision_state';
        END IF;
    END IF;

    SELECT *
    INTO override
    FROM "Acidbase".extract_object(object_id, structure, NULL);

    IF jsonb_typeof(override->'fields') = 'object' THEN
        obj := obj || jsonb_build_object('fields', (obj->'fields') || (override->'fields'));
    END IF;

    IF revision_id IS NULL AND override ? 'collections' AND jsonb_typeof(override->'collections') = 'object' THEN
        collections := '{}'::jsonb;
        IF obj ? 'collections' AND jsonb_typeof(obj->'collections') = 'object' THEN
            collections := obj->'collections';
        END IF;
        
        obj := obj || jsonb_build_object('collections', collections || override->'collections');
    END IF;

    RETURN obj;
END;

$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".extract_collection_1(IN bigint,IN bigint,IN jsonb) OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".extract_entity | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".extract_entity(IN bigint,IN jsonb,IN "Acidbase"."EXTRACTION_STATE") CASCADE;
CREATE FUNCTION "Acidbase".extract_entity (IN p_entity_id bigint, IN structure jsonb, IN state "Acidbase"."EXTRACTION_STATE" DEFAULT 'Latest')
	RETURNS jsonb
	LANGUAGE plpgsql
	VOLATILE 
	RETURNS NULL ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
    revision_id bigint;
    states "Acidbase"."REVISION_STATE"[];
BEGIN
    states[1] := 'Active';
    IF "state" = 'Latest' THEN
        states[2] := 'Deleted';
    ELSE
    END IF;

    SELECT object_id
    INTO revision_id
    FROM "Acidbase".revision
    WHERE
        entity_id = p_entity_id
    AND
        revision_state = ANY(states);

    RETURN "Acidbase".extract_revision(revision_id, "structure");
END;

$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".extract_entity(IN bigint,IN jsonb,IN "Acidbase"."EXTRACTION_STATE") OWNER TO postgres;
-- ddl-end --

-- object: public.get_config | type: FUNCTION --
-- DROP FUNCTION IF EXISTS public.get_config(IN text,IN text) CASCADE;
CREATE FUNCTION public.get_config (IN name text, IN default_value text)
	RETURNS text
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
    v text;
BEGIN
    SELECT current_setting(name)::text
    INTO v;
    RETURN v;

    EXCEPTION
        WHEN others
        THEN NULL;

    RETURN default_value;
END;
$$;
-- ddl-end --
ALTER FUNCTION public.get_config(IN text,IN text) OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".index_structure | type: TABLE --
-- DROP TABLE IF EXISTS "Acidbase".index_structure CASCADE;
CREATE TABLE "Acidbase".index_structure(
	name varchar(32) NOT NULL,
	index boolean NOT NULL DEFAULT false,
	read_structure jsonb NOT NULL,
	write_structure jsonb NOT NULL,
	version integer NOT NULL DEFAULT 1,
	script varchar(255),
	CONSTRAINT index_structure_pk PRIMARY KEY (name)

);
-- ddl-end --
ALTER TABLE "Acidbase".index_structure OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase"."ACTION_TYPE" | type: TYPE --
-- DROP TYPE IF EXISTS "Acidbase"."ACTION_TYPE" CASCADE;
CREATE TYPE "Acidbase"."ACTION_TYPE" AS
 ENUM ('Add','Delete','Edit','Revise','Purge','Revive');
-- ddl-end --
ALTER TYPE "Acidbase"."ACTION_TYPE" OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".queue_structure | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".queue_structure() CASCADE;
CREATE FUNCTION "Acidbase".queue_structure ()
	RETURNS trigger
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
    rec record;
BEGIN
    IF TG_OP = 'UPDATE' THEN
        rec := NEW;
    ELSE
        IF TG_OP = 'INSERT' THEN
            rec := NEW;
        ELSE
            rec := OLD;
        END IF;
    END IF;

    IF "public".get_config('acidbase.external_indexing', 'false') = 'true' THEN
        IF TG_OP = 'UPDATE' THEN
            IF OLD."index" THEN
                INSERT INTO "Acidbase".change_queue
                    (entity_id, revision_id, "action", index_name)
                VALUES
                    (0, 0, 'DELETE', OLD.name);
            END IF;

            IF NEW."index" THEN
                INSERT INTO "Acidbase".change_queue
                    (entity_id, revision_id, "action", index_name)
                VALUES
                    (0, 0, 'INSERT', NEW.name);

                PERFORM "Acidbase".enqueue_entities(NEW.name, NEW.read_structure->>'entity_type', NEW.read_structure->>'revision_state');
            END IF;
        ELSE
            IF TG_OP = 'INSERT' THEN
                IF NEW."index" THEN
                    INSERT INTO "Acidbase".change_queue
                        (entity_id, revision_id, "action", index_name)
                    VALUES
                        (0, 0, 'INSERT', NEW.name);
                        
                    PERFORM "Acidbase".enqueue_entities(NEW.name, NEW.read_structure->>'entity_type', NEW.read_structure->>'revision_state');
                END IF;
            ELSE
                IF OLD."index" THEN
                    INSERT INTO "Acidbase".change_queue
                        (entity_id, revision_id, "action", index_name)
                    VALUES
                        (0, 0, 'DELETE', OLD.name);
                    END IF;
            END IF;
        END IF;
    END IF;

    RETURN rec;
END;

$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".queue_structure() OWNER TO postgres;
-- ddl-end --

-- object: queue_structure | type: TRIGGER --
-- queue_structure ON "Acidbase".index_structure CASCADE;
CREATE CONSTRAINT TRIGGER queue_structure
	AFTER INSERT OR DELETE OR UPDATE
	ON "Acidbase".index_structure
	DEFERRABLE INITIALLY DEFERRED
	FOR EACH ROW
	EXECUTE PROCEDURE "Acidbase".queue_structure();
-- ddl-end --

-- object: "Acidbase".entity_structure | type: TABLE --
-- DROP TABLE IF EXISTS "Acidbase".entity_structure CASCADE;
CREATE TABLE "Acidbase".entity_structure(
	name varchar(32) NOT NULL,
	structure jsonb NOT NULL,
	version integer NOT NULL DEFAULT 1,
	CONSTRAINT index_structure_pk_1 PRIMARY KEY (name)

);
-- ddl-end --
ALTER TABLE "Acidbase".entity_structure OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".extract_object_forward_structure | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".extract_object_forward_structure(IN bigint) CASCADE;
CREATE FUNCTION "Acidbase".extract_object_forward_structure (IN p_object_id bigint)
	RETURNS jsonb
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
    rec record;
    _fields jsonb;
    object_fields jsonb;
    item_fields jsonb;
    _collections jsonb := '{}'::jsonb;
BEGIN
    SELECT fields
    INTO object_fields
    FROM "Acidbase".object
    WHERE
        object_id = p_object_id;

    item_fields := '{}'::jsonb;
    FOR rec IN
    SELECT fields
    FROM "Acidbase".item
    WHERE
        collection_id = (( SELECT collection_id FROM "Acidbase".item WHERE object_id = p_object_id ))
    LOOP
        item_fields := item_fields || rec.fields;
    END LOOP;

    IF object_fields IS NULL AND item_fields IS NULL THEN
        _fields := '[]'::jsonb;
    ELSEIF object_fields IS NOT NULL AND item_fields IS NULL THEN
        SELECT jsonb_agg(jsonb_object_keys)
        INTO _fields
        FROM jsonb_object_keys(object_fields);
    ELSEIF object_fields IS NULL AND item_fields IS NOT NULL THEN
        SELECT jsonb_agg(jsonb_object_keys)
        INTO _fields
        FROM jsonb_object_keys(item_fields);
    ELSE
        _fields := object_fields || item_fields;
        SELECT jsonb_agg(jsonb_object_keys)
        INTO _fields
        FROM jsonb_object_keys(_fields);
    END IF;

    IF _fields IS NULL THEN
        _fields := '[]'::jsonb;
    END IF;

    FOR rec IN
    SELECT c.name, MIN(i.object_id) AS sample_object_id
    FROM "Acidbase".collection c
    LEFT JOIN "Acidbase".item i ON (c.collection_id = i.collection_id)
    WHERE
        c.object_id = p_object_id
    GROUP BY c.name
    LOOP
        _collections := _collections || jsonb_build_object(rec.name, ((SELECT * FROM "Acidbase".extract_object_forward_structure(rec.sample_object_id))) );
    END LOOP;
    
    RETURN jsonb_build_object('fields', _fields, 'collections', _collections);
END;

$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".extract_object_forward_structure(IN bigint) OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".delete_entity | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".delete_entity(IN bigint,IN bigint) CASCADE;
CREATE FUNCTION "Acidbase".delete_entity (IN p_entity_id bigint, IN p_user_id bigint)
	RETURNS "Acidbase"."ENTITY_REVISION_INFO"
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
    rec record;
    eri "Acidbase"."ENTITY_REVISION_INFO";
    updated_count bigint;
    revision_id bigint;
    structure jsonb;
    data jsonb;
BEGIN
    WITH rows AS (
        UPDATE "Acidbase".revision
        SET
            revision_state = 'Obsolete'
        WHERE
            entity_id = p_entity_id
        AND
            revision_state = 'Active'
        RETURNING object_id
    )
    SELECT COUNT(*), MIN(object_id)
    INTO updated_count, revision_id
    FROM rows;

raise notice 'updated_count: %, revision_id: %', updated_count, revision_id;
    
    IF updated_count > 0 THEN
        SELECT *
        INTO structure
        FROM "Acidbase".extract_object_forward_structure(revision_id);
raise notice 'structure: %', structure::text;

        SELECT *
        INTO data
        FROM "Acidbase".extract_revision(revision_id, structure);
raise notice 'data: %', data::text;

        SELECT
            entity_id,
            creator,
            create_date,
            entity_type
        INTO
            eri.entity_id,
            eri.creator,
            eri.create_date,
            eri.entity_type
        FROM "Acidbase".entity
        WHERE
            entity_id = p_entity_id;

        INSERT INTO "Acidbase".revision
            (object_id, entity_id, reviser, revise_date, revision_state, fields)
        VALUES
            (DEFAULT, p_entity_id, p_user_id, DEFAULT, 'Deleted', data->'fields');

        eri.revision_id := LASTVAL();
        SELECT
            reviser,
            revise_date,
            revision_state
        INTO
            eri.reviser,
            eri.revise_date,
            eri.revision_state
        FROM "Acidbase".revision
        WHERE
            object_id = eri.revision_id;

        IF data ? 'collections' AND jsonb_typeof(data->'collections') = 'array' THEN
            FOR rec IN SELECT key, value FROM jsonb_each(data->'collections') AS t
            LOOP
                IF jsonb_typeof(rec.value) = 'array' THEN
                    PERFORM "Acidbase".store_collection(eri.revision_id, rec.key, rec.value);
                END IF;
            END LOOP;
        END IF;
        
        RETURN eri;
    ELSE
        RETURN NULL;
    END IF;
END;

$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".delete_entity(IN bigint,IN bigint) OWNER TO postgres;
-- ddl-end --

-- object: public.jsonb_object_delete_keys | type: FUNCTION --
-- DROP FUNCTION IF EXISTS public.jsonb_object_delete_keys(IN jsonb,VARIADIC text[]) CASCADE;
CREATE FUNCTION public.jsonb_object_delete_keys (IN obj jsonb, VARIADIC keys_to_delete text[])
	RETURNS jsonb
	LANGUAGE plpgsql
	IMMUTABLE 
	STRICT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN
    RETURN COALESCE(
        (
            SELECT jsonb_object_agg("key", "value")
            FROM jsonb_each("obj")
            WHERE "key" <> ALL ("keys_to_delete")
        ),
        '{}'::jsonb
    );
END;

$$;
-- ddl-end --
ALTER FUNCTION public.jsonb_object_delete_keys(IN jsonb,VARIADIC text[]) OWNER TO postgres;
-- ddl-end --

-- object: "inxOidName" | type: INDEX --
-- DROP INDEX IF EXISTS "Acidbase"."inxOidName" CASCADE;
CREATE INDEX "inxOidName" ON "Acidbase".collection
	USING btree
	(
	  name ASC NULLS LAST,
	  object_id ASC NULLS LAST
	);
-- ddl-end --

-- object: "inxCollectionId" | type: INDEX --
-- DROP INDEX IF EXISTS "Acidbase"."inxCollectionId" CASCADE;
CREATE INDEX "inxCollectionId" ON "Acidbase".item
	USING btree
	(
	  collection_id
	);
-- ddl-end --

-- object: "inxEidState" | type: INDEX --
-- DROP INDEX IF EXISTS "Acidbase"."inxEidState" CASCADE;
CREATE INDEX "inxEidState" ON "Acidbase".revision
	USING btree
	(
	  entity_id,
	  revision_state ASC NULLS LAST
	);
-- ddl-end --

-- object: "inxRid" | type: INDEX --
-- DROP INDEX IF EXISTS "Acidbase"."inxRid" CASCADE;
CREATE INDEX "inxRid" ON "Acidbase".item
	USING btree
	(
	  revision_id
	);
-- ddl-end --

-- object: "Acidbase".change_queue | type: TABLE --
-- DROP TABLE IF EXISTS "Acidbase".change_queue CASCADE;
CREATE TABLE "Acidbase".change_queue(
	id bigserial NOT NULL,
	entity_id bigint NOT NULL,
	revision_id bigint NOT NULL,
	action varchar(32) NOT NULL,
	index_name varchar(32) NOT NULL,
	created timestamp NOT NULL DEFAULT current_timestamp,
	CONSTRAINT change_queue_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE "Acidbase".change_queue OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".enqueue_entities | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".enqueue_entities(IN varchar(32),IN varchar(32),IN varchar(16)) CASCADE;
CREATE FUNCTION "Acidbase".enqueue_entities (IN _index_name varchar(32), IN _entity_type varchar(32), IN _revision_state varchar(16))
	RETURNS void
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN
    IF _revision_state = 'LatestActive' THEN

        INSERT INTO "Acidbase".change_queue
            (entity_id, revision_id, "action", index_name)
        SELECT
            r.entity_id, r.object_id, 'INSERT', _index_name
        FROM "Acidbase".revision r
        INNER JOIN "Acidbase".entity e ON (r.entity_id = e.entity_id)
        WHERE
            e.entity_type = _entity_type
        AND
            r.revision_state = 'Active';

    ELSEIF _revision_state = 'Latest' THEN

        INSERT INTO "Acidbase".change_queue
            (entity_id, revision_id, "action", index_name)
        SELECT
            r.entity_id, r.object_id, 'INSERT', _index_name
        FROM "Acidbase".revision r
        INNER JOIN "Acidbase".entity e ON (r.entity_id = e.entity_id)
        WHERE
            e.entity_type = _entity_type
        AND
            r.revision_state IN ('Active', 'Deleted');
            

    ELSEIF _revision_state = 'Revision' THEN

        INSERT INTO "Acidbase".change_queue
            (entity_id, revision_id, "action", index_name)
        SELECT
            r.entity_id, r.object_id, 'INSERT', _index_name
        FROM "Acidbase".revision r
        INNER JOIN "Acidbase".entity e ON (r.entity_id = e.entity_id)
        WHERE
            e.entity_type = _entity_type;

    END IF;
END;

$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".enqueue_entities(IN varchar(32),IN varchar(32),IN varchar(16)) OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".queue_entity | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".queue_entity() CASCADE;
CREATE FUNCTION "Acidbase".queue_entity ()
	RETURNS trigger
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN
    IF "public".get_config('acidbase.external_indexing', 'false') = 'true' THEN
        INSERT INTO "Acidbase".change_queue
            (entity_id, revision_id, "action", index_name)
        VALUES
            (OLD.entity_id, 0, 'Purge', '');
    END IF;

    RETURN OLD;
END;

$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".queue_entity() OWNER TO postgres;
-- ddl-end --

-- object: queue_object | type: TRIGGER --
-- DROP TRIGGER IF EXISTS queue_object ON "Acidbase".entity CASCADE;
CREATE TRIGGER queue_object
	AFTER DELETE 
	ON "Acidbase".entity
	FOR EACH ROW
	EXECUTE PROCEDURE "Acidbase".queue_entity();
-- ddl-end --

-- object: "Acidbase".find_collection_owner | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".find_collection_owner(IN bigint) CASCADE;
CREATE FUNCTION "Acidbase".find_collection_owner (IN _collection_id bigint, OUT _entity_id bigint, OUT _revision_id bigint)
	RETURNS record
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
    _parent_collection_id bigint;
BEGIN
    SELECT
        r.entity_id, r.object_id
    INTO
        _entity_id, _revision_id
    FROM "Acidbase".collection c
    INNER JOIN "Acidbase".revision r ON (c.object_id = r.object_id)
    WHERE
        c.collection_id = _collection_id;

    IF _entity_id IS NULL THEN
        SELECT
            i.collection_id
        INTO
            _parent_collection_id
        FROM "Acidbase".collection c
        INNER JOIN "Acidbase".item i ON (c.object_id = i.object_id)
        WHERE
            c.collection_id = _collection_id;
        
        SELECT
            t._entity_id, t._revision_id
        INTO
            _entity_id, _revision_id
        FROM "Acidbase".find_collection_owner(_parent_collection_id) t;
    END IF;
END;

$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".find_collection_owner(IN bigint) OWNER TO postgres;
-- ddl-end --

-- object: "Acidbase".merge_queues | type: FUNCTION --
-- DROP FUNCTION IF EXISTS "Acidbase".merge_queues() CASCADE;
CREATE FUNCTION "Acidbase".merge_queues ()
	RETURNS trigger
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
    rec record;
BEGIN
    IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
        rec := NEW;
    ELSE
        rec := OLD;
    END IF;

    PERFORM n.nspname, c.relname
    FROM pg_catalog.pg_class c
    LEFT JOIN pg_catalog.pg_namespace n ON (n.oid = c.relnamespace)
    WHERE
        n.nspname LIKE 'pg_temp_%'
    AND
        pg_catalog.pg_table_is_visible(c.oid)
    AND
        UPPER(relname) = UPPER('change_queue_tmp');

    IF FOUND THEN
        INSERT INTO "Acidbase".change_queue
            (entity_id, revision_id, action, index_name)
        SELECT
            entity_id, revision_id, action, ''
        FROM change_queue_tmp;

        DROP TABLE IF EXISTS change_queue_tmp;
    END IF;

    RETURN rec;
END;

$$;
-- ddl-end --
ALTER FUNCTION "Acidbase".merge_queues() OWNER TO postgres;
-- ddl-end --

-- object: merge_queues | type: TRIGGER --
-- merge_queues ON "Acidbase".revision CASCADE;
CREATE CONSTRAINT TRIGGER merge_queues
	AFTER INSERT OR UPDATE
	ON "Acidbase".revision
	DEFERRABLE INITIALLY DEFERRED
	FOR EACH ROW
	EXECUTE PROCEDURE "Acidbase".merge_queues();
-- ddl-end --

-- object: merge_queues | type: TRIGGER --
-- merge_queues ON "Acidbase".item CASCADE;
CREATE CONSTRAINT TRIGGER merge_queues
	AFTER INSERT OR DELETE OR UPDATE
	ON "Acidbase".item
	DEFERRABLE INITIALLY DEFERRED
	FOR EACH ROW
	EXECUTE PROCEDURE "Acidbase".merge_queues();
-- ddl-end --


-- Appended SQL commands --
CREATE EXTENSION pgcrypto;
---
